import gnp # https://pypi.python.org/pypi/gnp
import pprint
import os
import time
import stat
import json
import datetime
import string
from newspaper import Article #https://pypi.python.org/pypi/newspaper
from flask import Flask,render_template, request
import redis
redis_cache = redis.StrictRedis(host='localhost', port=6379, db=0)


gn_json_file="gn.json"
gn_query="government website"



def get_new_gn_json():
	if test_json_age() > 3000:
		gn_results=gnp.get_google_news_query(gn_query)
		with open(gn_json_file, 'w') as outfile:
	    		json.dump(gn_results, outfile)

def test_json_age():
	try:
		seconds_old=time.time() - os.stat(gn_json_file)[stat.ST_MTIME]
	except:
		seconds_old=9999
	return int(seconds_old)

def gn_json_obj():
	get_new_gn_json()
	with open(gn_json_file) as json_data:
    		gn_data = json.load(json_data)
    		json_data.close()
    		return gn_data

def print_gn_json():
	dir(gn_json_obj)
	pprint.pprint(gn_json_obj())

def print_stories():
		gn_data=gn_json_obj()
		number_of_stories=len(gn_data["stories"])
		for story_num in range(number_of_stories):
			for story_section in ("title", "source", "link","content_snippet"):
				print gn_data['stories'][story_num][story_section]
			print
def get_article_info(link):
	if redis_cache.get(link):
		#print string.split(redis_cache.get(link),',')[0]
		#return string.split(redis_cache.get(link),',')
		return json.loads(redis_cache.get(link))

	else:
		article = Article(link)
		article.download()
		article.parse()

		article_text=article.text
		article_publish_date=article.publish_date

		try:
			story_date=datetime.date.strftime(article_publish_date,'%Y-%m-%d')
		except TypeError:
			story_date=datetime.date.strftime(datetime.date.today(),'%Y-%m-%d')

		cache_json=json.dumps([article_text,story_date])
		redis_cache.set(link,cache_json)

		return (article_text,story_date)

app = Flask(__name__)

@app.route("/review.html" ,methods=['GET', 'POST'])
def review_stories():
	selected_stories=[]
	story_text={}
	story_dates={}
	gn_data=gn_json_obj()
	stories=gn_data['stories']

	for story in stories:
		if story['title'] in request.form:
			selected_stories.append(story)

	for selected_story in selected_stories:
		story_text[selected_story['title']],story_date=get_article_info(selected_story['link'])
		story_dates[selected_story['title']]=story_date
	#print story_text
	return render_template('review.html',
		stories=selected_stories,
		story_text=story_text,
		story_dates=story_dates,
		date_str=datetime.date.strftime(datetime.date.today(),'%A %Y-%m-%d')
		)


@app.route("/")
def pick_stories():
	gn_data=gn_json_obj()
	stories=gn_data['stories']
	return render_template('stories.html', stories=stories)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
